module gitee.com/drololo/simpleData

go 1.17

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	gorm.io/gorm v1.23.5 // indirect
)
