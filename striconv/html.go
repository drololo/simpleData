// Package striconv -----------------------------
// @file      : html.go
// @author    : JuneXu
// @contact   : 428192774@qq.com
// @time      : 2022/6/22 14:03
// -------------------------------------------
package striconv

import (
	"regexp"
	"strings"
)

//去除字符串中的html标签
func TrimHtmlMark(src string) string {
	//将HTML标签全转换成小写
	re, _ := regexp.Compile("\\<[\\S\\s]+?\\>")
	src = re.ReplaceAllStringFunc(src, strings.ToLower)
	//去除STYLE
	re, _ = regexp.Compile("\\<style[\\S\\s]+?\\</style\\>")
	src = re.ReplaceAllString(src, "")
	//去除SCRIPT
	re, _ = regexp.Compile("\\<script[\\S\\s]+?\\</script\\>")
	src = re.ReplaceAllString(src, "")
	//去除所有尖括号内的HTML代码，并换成换行符
	re, _ = regexp.Compile("\\<[\\S\\s]+?\\>")
	src = re.ReplaceAllString(src, "\n")
	//去除连续的换行符
	re, _ = regexp.Compile("\\s{2,}")
	src = re.ReplaceAllString(src, "\n")
	return strings.TrimSpace(src)
}

func TrimHtmlScript(src string) string {
	//转义scrpit标签的符号
	re, _ := regexp.Compile("\\<script")
	src = re.ReplaceAllString(src, "&lt;script")
	re, _ = regexp.Compile("script\\>")
	src = re.ReplaceAllString(src, "script&gt;")
}
