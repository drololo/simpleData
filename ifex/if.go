/*
 * @FileName:   if.go
 * @Author:		JuneXu
 * @CreateTime:	2022/3/31 下午10:34
 * @Description:
 */

package ifex

func If(condition bool, trueVal, falseVal interface{}) interface{} {
	if condition {
		return trueVal
	}
	return falseVal
}
