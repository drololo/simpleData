// Package struconv -----------------------------
// @file      : struct_test.go
// @author    : JuneXu
// @contact   : 428192774@qq.com
// @time      : 2022/6/2 23:37
// -------------------------------------------
package struconv

import (
	"testing"
)

type demoTable struct {
	Name string `json:"name" form:"name11" gorm:"column:name;comment:名称"`
	Age  int    `json:"age" form:"age11" gorm:"column:age;comment:年龄"`
}

func TestToGormCommentList(t *testing.T) {
	var a = demoTable{}
	b := ToGormCommentList(a)
	t.Log(b)
}

func TestToTagKeyList(t *testing.T) {
	var a = demoTable{}
	b := ToTagKeyList(a, "gorm", ";", "COMMENT")
	t.Log(b)
}

func TestToTagFieldList(t *testing.T) {
	var a = demoTable{}
	b := ToTagFieldList(a, "json")
	t.Log(b)
}

func TestGetTagVaule(t *testing.T) {
	var a = demoTable{}
	b := GetTagVaule(a, "Name", "gorm")
	t.Log(b)
}
