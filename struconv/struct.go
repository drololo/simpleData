/*
 * @FileName:   dataHandler.go
 * @Author:		xjj
 * @CreateTime:	2021/12/20 下午4:28
 * @Description: 处理数据常用方法封装
 */
package struconv

import (
	"encoding/json"
	"fmt"
	"gitee.com/drololo/simpleData/striconv"
	"reflect"
	"strings"
)

//
//  ToString
//  @Description: 结构体转字符串
//  @param array 切片或数组
//  @param operator 赋值符号 如"=",":"等
//  @param spacer 间隔符 如"," 、 "&" 、 " "等
//  @return data
//
func ToString(array interface{}, operator, spacer string) (data string) {
	rev := reflect.ValueOf(array)
	ret := reflect.TypeOf(array)
	fieldNum := ret.NumField()
	for i := 0; i < fieldNum; i++ {
		if data == "" {
			data = fmt.Sprintf("%v%v%v", striconv.LowerCapital(ret.Field(i).Name), operator, rev.Field(i))
		} else {
			data = fmt.Sprintf("%v%v%v%v%v", data, spacer, striconv.LowerCapital(ret.Field(i).Name), operator, rev.Field(i))
		}
	}
	return
}

/**
@Function:ToMapUseField
@Description:结构体转map 使用的是结构体字段（字段首字母会小写）
@Param:obj interface{} 结构体对象,!!不支持带有私有属性的结构体(字段首字母需要大写)!!
@Return:map[string]interface{}
@author:JunjieXu
@Time:2021/12/23
*/
func ToMapUseField(obj interface{}) (result map[string]interface{}) {
	ret := reflect.TypeOf(obj)
	rev := reflect.ValueOf(obj)
	result = make(map[string]interface{})
	for i := 0; i < ret.NumField(); i++ {
		if rev.Field(i).CanInterface() {
			result[striconv.LowerCapital(ret.Field(i).Name)] = rev.Field(i).Interface()
		}
	}
	return
}

/**
@Function:ToMap
@Description:使用指定tag标签将结构体转map,如果tag不存在，则将字段名首字母小写后作为key。
@Param:obj interface{} 结构体对象，!!不支持带有私有属性的结构体(字段首字母需要大写)!!
@Param:tag string 标签
@Return:map[string]interface{}
@author:JunjieXu
@Time:2021/3/2
*/
func ToMap(data interface{}, tag string) (result map[string]interface{}) {
	switch tag {
	case "json":
		if jsonData, err := json.Marshal(&data); err != nil {
			return
		} else {
			err = json.Unmarshal(jsonData, &result)
		}
	default:
		ret := reflect.TypeOf(data)
		rev := reflect.ValueOf(data)
		result = make(map[string]interface{})
		for i := 0; i < ret.NumField(); i++ {
			if rev.Field(i).CanInterface() {
				field := ret.Field(i).Tag.Get(tag)
				if field == "" {
					field = striconv.LowerCapital(ret.Field(i).Name)
				}
				result[field] = rev.Field(i).Interface()
			}
		}
	}
	return
}

//使用json标签将结构体转换为http请求的prams字符串结构
func ToQueryParam(data interface{}, tag string) (result string) {
	t := reflect.TypeOf(data)
	v := reflect.ValueOf(data)
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i).Tag.Get(tag)
		if field == "" {
			field = striconv.LowerCapital(t.Field(i).Name)
		}
		if tag == "json" {
			field = strings.Replace(field, ",omitempty", "", 1)
		}
		//忽略空值
		if v.Field(i).String() == "" {
			continue
		}
		result = fmt.Sprintf("%v&%v=%v", result, field, v.Field(i))
	}
	result = result[1:len(result)]
	return
}
