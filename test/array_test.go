/*
 * @FileName:   array_test.go
 * @Author:		JuneXu
 * @CreateTime:	2022/3/31 下午8:33
 * @Description:
 */

package test

import (
	"strings"
	"testing"
)

type user struct {
	Name string
	Age  uint
}

func TestArrayToString(t *testing.T) {
	var array1 = []string{"111", "222"}
	data := arrconv.ToString(array1)
	t.Log(data)
	//"111,222"

	var struc = struct {
		Name string
		Age  int
		Like []string
	}{"zhangsan", 22, []string{"cat", "boy"}}

	data2 := arrconv.ToString(struc)
	t.Log(data2)
	//"{zhangsan,22,[cat,boy]}"

	var a1 = []user{
		{"张三", 22},
		{"李逵", 11},
		{"诸葛亮", 15},
	}
	t.Log(arrconv.ToString(a1))
	//"{张三,22},{李逵,11},{诸葛亮,15}"
}

func TestArray_IsDataInStructList(t *testing.T) {
	var a1 = []user{
		{"张三", 22},
		{"李逵", 11},
		{"诸葛亮", 15},
	}
	var b1 = user{"李逵", 15}
	t.Log(arrconv.IsDataInStructList(a1, b1, []string{"Name"}))
	//1 true

	t.Log(arrconv.IsDataInStructList(a1, b1, nil))
	//-1 false
}

func TestArray_DiffList(t *testing.T) {
	var a1 = []user{
		{"张三", 22},
		{"李逵", 11},
		{"诸葛亮", 15},
	}
	var a2 = []user{
		{"张三", 15},
		{"tom", 11},
		{"alex", 15},
	}
	//单字段匹配
	diff, same, err := arrconv.DiffList(a1, a2, "Name")
	t.Log(diff) //[{李逵 11} {诸葛亮 15}]
	t.Log(same) //[{张三 15}]
	t.Log(err)  //true

	t.Log(strings.Repeat("=", 50))
	//多字段匹配
	diff, same, err = arrconv.DiffList(a1, a2, []string{"Name", "Age"})
	t.Log(diff) //[{张三 22} {李逵 11} {诸葛亮 15}]
	t.Log(same) //[]
	t.Log(err)  //true

	t.Log(strings.Repeat("=", 50))
	//全字段匹配
	diff, same, err = arrconv.DiffList(a1, a2, nil)
	t.Log(diff) //[{张三 22} {李逵 11} {诸葛亮 15}]
	t.Log(same) //[]
	t.Log(err)  //true
}

func TestStructListToMap(t *testing.T) {
	var a1 = []user{
		{"张三", 22},
		{"李逵", 11},
		{"诸葛亮", 15},
	}
	t.Log(arrconv.StructListToMap(a1, "Name"))
	//map[张三:{张三 22} 李逵:{李逵 11} 诸葛亮:{诸葛亮 15}] true

	t.Log(arrconv.StructListToMap(a1, nil))
	//map[张三-22:{张三 22} 李逵-11:{李逵 11} 诸葛亮-15:{诸葛亮 15}] true

	t.Log(arrconv.StructListToMap(a1, []string{"Name", "Age"}))
	//map[张三-22:{张三 22} 李逵-11:{李逵 11} 诸葛亮-15:{诸葛亮 15}] true
}
