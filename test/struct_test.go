package test

import (
	"fmt"
	"gitee.com/drololo/simpleData/struconv"
	"testing"
)

func TestStructToString(t *testing.T) {
	var ori = struct {
		A string
		B string
	}{"aaa", "bbb"}
	t.Log(struconv.ToString(ori, "=", "&"))
}

func TestStructToMap(t *testing.T) {
	//不支持的方法，里面的字段需要首字母大写
	var a = struct {
		User string
		name string
	}{
		"张三", "王德发",
	}
	data := struconv.ToMap(a, "json")
	fmt.Println(data)
}

//cannot return value obtained from unexported field or method [recovered]
func TestStructToMapUseTag(t *testing.T) {
	var a = struct {
		User string `json:"user"`
		Name string `json:"name,omitempty"`
	}{
		"张三", "",
	}
	data := struconv.ToMap(a, "json")
	fmt.Println(data)
}

func TestToQueryParam(t *testing.T) {
	var a = struct {
		User string `json:"user"`
		Name string `json:"name,omitempty"`
	}{
		"张三", "",
	}
	data := struconv.ToQueryParam(a, "json")
	fmt.Println(data)
}
