// Package excel -----------------------------
// @file      : rc.go
// @author    : JuneXu
// @contact   : 428192774@qq.com
// @time      : 2022/6/3 10:46
// -------------------------------------------
package excel

import (
	"fmt"
)

//
//  GetEcelAxis
//  @Description: 获取excel表中单元格的索引
//  @param row:行数
//  @param column:列数
//  @return string
//
//行列坐标值转换为excel的坐标。注意row和columnCount的初始值都是1
func GetEcelAxis(row int, columnCount int) string {
	var column = getColumnIndex(columnCount)
	return fmt.Sprintf("%s%d", column, row)
}

//获取excel的列索引
var columnIndexList = []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}

func getColumnIndex(num int) string {
	num--
	var column = columnIndexList[num%26]
	for num = num / 26; num > 0; num = num/26 - 1 {
		column = columnIndexList[(num-1)%26] + column
	}
	return column
}
