// Package excel -----------------------------
// @file      : rc_test.go
// @author    : JuneXu
// @contact   : 428192774@qq.com
// @time      : 2022/6/27 16:17
// -------------------------------------------
package excel

import (
	"testing"
)

func TestGetCellIndex(t *testing.T) {
	t.Log(getColumnIndex(26))   //Z
	t.Log(getColumnIndex(27))   //AA
	t.Log(getColumnIndex(676))  //YZ
	t.Log(getColumnIndex(702))  //ZZ
	t.Log(getColumnIndex(1352)) //AYZ
	//t.Log(getColumnIndex(1352)) //AYZ
	t.Log(ConvertNumToChars(26))
	t.Log(ConvertNumToChars(27))
	t.Log(ConvertNumToChars(676))
	t.Log(ConvertNumToChars(702))
	t.Log(ConvertNumToChars(1352))
}

var ExcelChar = []string{"", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}

func ConvertNumToChars(num int) (string, error) {
	var cols string
	v := num
	for v > 0 {
		k := v % 26
		if k == 0 {
			k = 26
		}
		v = (v - k) / 26
		cols = ExcelChar[k] + cols
	}
	return cols, nil
}

func ConvertNumToChar(num int) (string, error) {
	if num < 27 {
		return ExcelChar[num], nil
	}
	k := num % 26
	if k == 0 {
		k = 26
	}
	v := (num - k) / 26
	col, err := ConvertNumToChar(v)
	if err != nil {
		return "", err
	}
	cols := col + ExcelChar[k]
	return cols, nil
}
