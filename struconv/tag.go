// Package struconv -----------------------------
// @file      : tag.go
// @author    : JuneXu
// @contact   : 428192774@qq.com
// @time      : 2022/6/3 0:35
// -------------------------------------------
package struconv

import (
	"reflect"
	"strings"
)

func ParseTagSetting(str string, sep string) map[string]string {
	settings := map[string]string{}
	names := strings.Split(str, sep)

	for i := 0; i < len(names); i++ {
		j := i
		if len(names[j]) > 0 {
			for {
				if names[j][len(names[j])-1] == '\\' {
					i++
					names[j] = names[j][0:len(names[j])-1] + sep + names[i]
					names[i] = ""
				} else {
					break
				}
			}
		}

		values := strings.Split(names[j], ":")
		k := strings.TrimSpace(strings.ToUpper(values[0]))

		if len(values) >= 2 {
			settings[k] = strings.Join(values[1:], ":")
		} else if k != "" {
			settings[k] = k
		}
	}

	return settings
}

//
//  GetTagVaule
//  @Description: 获取tag标签的值
//  @param structData
//  @param structField
//  @param tagName
//  @return string
//
func GetTagVaule(structData interface{}, structField, tagName string) string {
	t := reflect.TypeOf(structData)
	for i := 0; i < t.NumField(); i++ {
		if t.Field(i).Name == structField {
			tag := t.Field(i).Tag.Get(tagName)
			return tag
		}
	}
	return ""
}

//
//  ToTagKeyList
//  @Description: 将结构体所有字段转换成以标签中的指定的key值为列表的切片.此功能的标签必须类似于`gorm:"column:id;comment:主键"`这种形式
//  @param data： 结构体数据
//  @param tagField: 标签中的字段 如json、form、gorm等
//  @param spacer： 字段中的key值之间的分隔符
//  @param fieldKey：字段中的key值
//  @return commentList
//
func ToTagKeyList(data interface{}, tagField, spacer, fieldKey string) (commentList []string) {
	t := reflect.TypeOf(data)
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i).Tag.Get(tagField)
		commentList = append(commentList, ParseTagSetting(field, spacer)[fieldKey])
	}
	return
}

//转换成以gorm标签中的commit注释名为列表的切片
func ToGormCommentList(data interface{}) (commentList []string) {
	t := reflect.TypeOf(data)
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i).Tag.Get("gorm")
		commentList = append(commentList, ParseTagSetting(field, ";")["COMMENT"])
	}
	return
}

func ToTagFieldList(data interface{}, tag string) (tagList []string) {
	t := reflect.TypeOf(data)
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i).Tag.Get(tag)
		tagList = append(tagList, field)
	}
	return
}
