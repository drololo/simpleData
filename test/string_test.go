/*
 * @FileName:   string_test.go
 * @Author:		JuneXu
 * @CreateTime:	2022/3/31 下午9:42
 * @Description:
 */

package test

import (
	"gitee.com/drololo/simpleData/striconv"
	"testing"
)

func TestUpperCapital(t *testing.T) {
	t.Log(striconv.UpperCapital("abbCd"))
	//AbbCd
}

func TestLowerCapital(t *testing.T) {
	t.Log(striconv.LowerCapital("AbbCCC"))
	//abbCCC
}

func TestStrconv_Base64ToString(t *testing.T) {
	var a = "slfjalkfj6461f3a54df3as1df68"
	bs64 := striconv.StringToBase64(a)
	t.Log(string(bs64))
	//c2xmamFsa2ZqNjQ2MWYzYTU0ZGYzYXMxZGY2OA==

	c := striconv.StringToBase64V2(a)
	t.Log(c)
	//c2xmamFsa2ZqNjQ2MWYzYTU0ZGYzYXMxZGY2OA==

	b, _ := striconv.Base64ToString(string(bs64))
	t.Log(string(b))
	//slfjalkfj6461f3a54df3as1df68

}
