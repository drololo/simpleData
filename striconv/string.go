/*
 * @FileName:   string.go
 * @Author:		JuneXu
 * @CreateTime:	2022/3/31 下午9:28
 * @Description:
 */

package striconv

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"strings"
)

//UpperCapital 首字母大写
func UpperCapital(str string) string {
	return fmt.Sprintf("%v%v", strings.ToUpper(str[:1]), str[1:])
}

//UpperCapital 首字母小写
func LowerCapital(str string) string {
	return fmt.Sprintf("%v%v", strings.ToLower(str[:1]), str[1:])
}

//base64数据转字符串
func Base64ToString(base64Data string) (result []byte, err error) {
	//sEnc := base64.StdEncoding.EncodeToString([]byte(base64Data))
	buff, err := base64.StdEncoding.DecodeString(base64Data)
	if err != nil {
		return nil, err
	}
	return buff, nil
}

//StringToBase64 字符串转base64 结果为byte
func StringToBase64(strData string) (result []byte) {
	var b bytes.Buffer
	w := base64.NewEncoder(base64.URLEncoding, &b)
	w.Write([]byte(strData))
	w.Close()
	result = b.Bytes()
	return
}

//StringToBase64 字符串转base64 结果为字符串
func StringToBase64V2(strData string) (result string) {
	return base64.StdEncoding.EncodeToString([]byte(strData))
}
