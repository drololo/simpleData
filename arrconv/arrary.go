/*
 * @FileName:   arrary.go
 * @Author:		JuneXu
 * @CreateTime:	2022/3/30 下午10:05
 * @Description:
 */

package arrconv

import (
	"fmt"
	"reflect"
	"strings"
)

//@author: [piexlmax](https://github.com/piexlmax)
//@function: ArrayToString
//@description: 将数组格式化为字符串
//@param: array []interface{}
//@return: string
func ToString(array interface{}) string {
	return strings.Replace(strings.Trim(fmt.Sprint(array), "[]"), " ", ",", -1)
}

/*
@Function:IsDataInStructList
@Description:判断数据是否在切片/列表中
@Param: datalist 类型：interface{}，传入切片值
@Param: data 类型：interface{}，传入要查询的数据,一般为与datalist中元素一直的结构体
@Param: choiceFields []string.需要对比的字段,注意这是一个切片. 传nil为完全对比
@Return: index int 数据在列表中的索引
@Return: result bool 数据是否在列表中
@author:JunjieXu
@Time:2021/12/22
*/
func IsDataInStructList(datalist interface{}, data interface{}, choiceFields []string) (index int, result bool) {
	interDatalist := reflect.ValueOf(datalist)
	compareValue := reflect.ValueOf(data)
	if interDatalist.Len() == 0 {
		return -1, false
	}
	//完全匹配
	if choiceFields == nil {
		for i := 0; i < interDatalist.Len(); i++ {
			if interDatalist.Index(i).Interface() == compareValue.Interface() {
				return i, true
			}
		}
	} else {
		//只匹配字段值
		for i := 0; i < interDatalist.Len(); i++ {
			fieldCheck := true
			for _, field := range choiceFields {
				//有一个字段值不匹配就失败
				if interDatalist.Index(i).FieldByName(field).Interface() != compareValue.FieldByName(field).Interface() {
					fieldCheck = false
				}
			}
			if fieldCheck == true {
				return i, true
			}
		}
	}
	return -1, false
}

func IsDataInStrList(data []string, item string) bool {
	for _, v := range data {
		if item == v {
			return true
		}
	}
	return false
}

/*
@Function:FindIndexInDataList
@Description:查询数据在切片/列表中的索引，就是Isdatainlist函数起得别名
@Param: datalist 类型：interface{}，传入切片值
@Param: data 类型：interface{}，传入要查询的数据,一般为与datalist中元素一直的结构体
@Param: fieldList []string.需要对比的字段,注意这是一个切片. 传nil为完全对比
@Return: index int 数据在列表中的索引
@Return: result bool 数据是否在列表中
@author:JunjieXu
@Time:2021/12/22
*/
func FindIndexInDataList(datalist interface{}, data interface{}, fieldList []string) (index int, result bool) {
	return IsDataInStructList(datalist, data, fieldList)
}

/**
@Function:DiffList
@Description: 与另一个结构体数组/切片对比，获取相同值和不同值。
@Param: currentList 当前的结构体数组/切片
@Param: beforeList 需要对比的结构体数字/切片
@Param: fields (string/list/slice/nil)对比哪几个字段，传nil为对比所有数据
@Return: diffList 不同部分
@Return: sameList 相同部分
@Return: ok true/false 成功/失败
@author:JunjieXu
@Time:2021/12/24
*/
func DiffList(currentList interface{}, beforeList interface{}, ChoiceFields interface{}) (diffList []interface{}, sameList []interface{}, ok bool) {
	m1, _ := StructListToMap(currentList, ChoiceFields)
	m2, _ := StructListToMap(beforeList, ChoiceFields)
	for key, _ := range m1 {
		if value, ok := m2[key]; ok == false {
			diffList = append(diffList, m1[key])
		} else {
			sameList = append(sameList, value)
		}
	}
	return diffList, sameList, true
}

/**
@Function:StructListToMap
@Description: 结构体列表转字典
@Param: obj （结构体对象）
@Param: fields （string/list/nil） 以哪几个字段的值组成key，传nil为所有
@Return: result map数据
@Return: ok  false/ture  失败/成功
@author:JunjieXu
@Time:2021/12/24
*/
func StructListToMap(obj interface{}, fields interface{}) (map[string]interface{}, bool) {
	var result = make(map[string]interface{})
	interVlaueObj := reflect.ValueOf(obj)
	switch fields.(type) {
	case string:
		//遍历切片获取结构体
		for i := 0; i < interVlaueObj.Len(); i++ {
			item := interVlaueObj.Index(i)
			//获取结构体字段值作为key
			key := item.FieldByName(fields.(string))
			result[key.String()] = item.Interface()
		}
	case []string:
		//遍历切片获取结构体
		for i := 0; i < interVlaueObj.Len(); i++ {
			item := interVlaueObj.Index(i)
			var fillKey string
			//取字段值组合为key
			for _, field := range fields.([]string) {
				value := item.FieldByName(field)
				fillKey = fmt.Sprintf("%v-%v", fillKey, value)
			}
			result[fillKey[1:]] = item.Interface()
		}
	case nil:
		//遍历切片获取结构体
		for i := 0; i < interVlaueObj.Len(); i++ {
			item := interVlaueObj.Index(i)
			var fillKey string
			//取所有的字段值组合为key
			for ii := 0; ii < item.NumField(); ii++ {
				fillKey = fmt.Sprintf("%v-%v", fillKey, item.Field(ii).Interface())
			}
			result[fillKey[1:]] = item.Interface()
		}
	}
	return result, true
}

//列表/切片的排序
func Sort(dataList interface{}, column interface{}, desc bool) {
	panic("暂未开发")
}

//列表合并
func Merge(...interface{}) {
	panic("暂未开发")
}

//列表反转
func Reverse(dataList interface{}) {
	panic("暂未开发")

}

//返回末尾的n个元素
func Tail(dataList interface{}, n int) {
	panic("暂未开发")

}

//返回开头的n个元素
func Head(dataList interface{}, n int) {
	panic("暂未开发")

}
